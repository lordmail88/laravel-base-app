<?php

namespace App\Http\Controllers\Maintenance;

use App\Repositories\ILogsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class LogsController extends Controller
{
    //
    protected $logsRepo;
    public function __construct(ILogsRepository $logsRepo)
    {
        $this->logsRepo = $logsRepo;
    }

    public function index(){
        return view('maintenance.logs.index', ['logs' => $this->logsRepo->getLogs()]);
    }

    public function show($log){
        return view('maintenance.logs.show', [
            'filename' => $log,
            'contents' => $this->logsRepo->getLog($log)
        ]);
    }
}
