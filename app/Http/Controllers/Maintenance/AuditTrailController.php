<?php

namespace App\Http\Controllers\Maintenance;

use App\Repositories\IAuditTrailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class AuditTrailController extends Controller
{
    //
    protected $auditTrailRepo;

    public function __construct(IAuditTrailRepository $auditTrailRepo)
    {
        $this->auditTrailRepo = $auditTrailRepo;
    }

    public function index()
    {
        Log::info($this->auditTrailRepo->getAllModels());
        return view('maintenance.audit_trail.index', [
           'models' => $this->auditTrailRepo->getAllModels()
        ]);

    }
}
