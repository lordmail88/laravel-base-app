<?php

namespace App\Http\Controllers\NeighborhoodCommunity;

use App\Models\Entity\Community;
use App\Repositories\ICommunityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class CommunityController extends Controller
{
    protected $communityRepo;

    public function __construct(ICommunityRepository $communityRepo)
    {
        $this->communityRepo = $communityRepo;
    }

    public static function getSessionKey()
    {
        return 'community_id';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->exists($this::getSessionKey()))
        {
            session()->pull($this::getSessionKey());
        }

        return view('neighborhood_community.community.index', [
            'models' => $this->communityRepo->allWithStatistics()
        ]);
    }

    /**
     * Display a datatables resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        //
        return $this->communityRepo->datatables();

    }

    /**
     * Display a select2 resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entity\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function show(Community $community)
    {
        session([
            $this::getSessionKey() => $community->id
        ]);
        return view('neighborhood_community.community.show', ['model' => $community]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Entity\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function edit(Community $community)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entity\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Community $community)
    {
        //
    }

    /**
     * Delete Information
     *
     * @param Community $community
     * @return \Illuminate\Http\Response
     */
    public function delete($community)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entity\Community  $community
     * @return \Illuminate\Http\Response
     */
    public function destroy(Community $community)
    {
        //
    }
}
