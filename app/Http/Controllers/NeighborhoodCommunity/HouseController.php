<?php

namespace App\Http\Controllers\NeighborhoodCommunity;

use App\Models\Entity\House;
use App\Repositories\IHouseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HouseController extends Controller
{
    protected $houseRepo;

    public function __construct(IHouseRepository $houseRepository)
    {
        $this->houseRepo = $houseRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!session()->exists(CommunityController::getSessionKey()))
        {
            return redirect(action('NeighborhoodCommunity\\CommunityController@index'));
        }
        return view('neighborhood_community.house.index');
    }

    /**
     * Display a datatables resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        //
        return $this->houseRepo->datatables();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Entity\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show(House $house)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Entity\House  $house
     * @return \Illuminate\Http\Response
     */
    public function edit(House $house)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Entity\House  $house
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, House $house)
    {
        //
    }

    /**
     * Delete Information
     *
     * @param Community $community
     * @return \Illuminate\Http\Response
     */
    public function delete($community) : Response
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Entity\House  $house
     * @return \Illuminate\Http\Response
     */
    public function destroy(House $house)
    {
        //
    }
}
