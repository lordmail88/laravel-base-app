<?php

namespace App\Http\Controllers\UserManagement;

use App\Http\Requests\UserManagement\User\CreateFormRequest;
use App\Models\Entity\User;
use App\Repositories\IUserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    protected $userRepo;

    public function __construct(IUserRepository $userRepository)
    {
        $this->userRepo = $userRepository;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('user_management.user.index');
    }

    /**
     * Display a datatables resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function datatables()
    {
        //
        return $this->userRepo->datatables();
    }

    /**
     * Display a select2 resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function select2()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("user_management.user.create");
    }

    /**
     * @param CreateFormRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFormRequest $request)
    {
        //
        $validated = $request->validated();
        $this->userRepo->saveOrUpdate(null,$request);
        return redirect()->action('UserManagement\UserController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function show($user)
    {
        //
        return view('user_management.user.show', ['model' => $user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
        //
    }

    /**
     * @param Request $request
     * @param User $user
     */
    public function update(Request $request, $user)
    {
        //
    }

    /**
     * Delete Information
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function delete($user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  string $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user)
    {
        //
    }
}
