<?php
namespace App\Traits;
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 6/29/2019
 * Time: 12:38 PM
 */

use Illuminate\Support\Str;

trait ModelUuid
{
    /**
     * Boot function from Laravel
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $model->incrementing = false;
            $model->keyType = 'string';
            $model->{$model->getKeyName()} = (string) Str::uuid();
        });
    }
}

