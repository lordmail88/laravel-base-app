<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 7/17/2019
 * Time: 10:31 AM
 */

namespace App\Repositories;


use App\Models\Entity\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use OwenIt\Auditing\Contracts\Auditable;
use OwenIt\Auditing\Models\Audit;

class AuditTrailRepository implements IAuditTrailRepository
{

    public function getAllModels()
    {
        $allModels = collect([]);
        $modelNamespace = 'App\\Models\\Entity';
        $models = File::allFiles(base_path($modelNamespace));
        foreach ($models as $model)
        {
//            $className = str_replace(base_path().'\\','',$model);
            $className = str_replace('.php','',$model->getRelativePathname());
            $modelName = str_replace($modelNamespace. '\\', '', $className);

            // is implement auditable ?
            $ref = new \ReflectionClass($modelNamespace .'\\'. $className);
            $isAudit = $ref->implementsInterface(Auditable::class);

            $allModels->push((object) [
                'fullname' => $className,
                'name' => $modelName,
                'isAudit' => $isAudit,
                'dataCount' => $this->getDataCount($modelNamespace .'\\'.$className),
                'auditCount' => $isAudit ? $this->getAuditCount($modelNamespace .'\\'.$className) : 0
            ]);
        }
        return $allModels->all();
    }

    public function getModel($model)
    {
        // TODO: Implement getModel() method.
    }

    public function getAuditTrail($model)
    {
        // TODO: Implement getAuditTrail() method.
    }

    public function getDataCount($model)
    {
        if(is_string($model))
        {
            return $model::count();
        }
    }

    public function getAuditCount($model)
    {
        if(is_string($model))
        {
            return $model::withCount('audits')->get()->map(function($item,$key){
                return $item->audits_count;
            })->sum();
        }
    }
}
