<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 7/18/2019
 * Time: 1:00 PM
 */

namespace App\Repositories;


interface ICommunityRepository extends IBaseCrudRepository
{
    public function allWithStatistics();
}
