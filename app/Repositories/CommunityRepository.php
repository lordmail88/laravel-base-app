<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 7/18/2019
 * Time: 1:00 PM
 */

namespace App\Repositories;


use App\Models\Entity\Community;
use Yajra\DataTables\Facades\DataTables;

class CommunityRepository implements ICommunityRepository
{

    public function all()
    {
        // TODO: Implement all() method.
        return Community::all();
    }

    public function datatables()
    {
        $data = Community::select([
            'id',
            'name'
        ]);

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($model)
            {
                return '<a href="'.action('NeighborhoodCommunity\CommunityController@show',['id'=> $model->id]).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a>';
            })
            ->make();
    }

    public function get($id)
    {
        // TODO: Implement get() method.
        return Community::findOrFail($id);
    }

    public function saveOrUpdate($id, \Illuminate\Http\Request $data)
    {
        // TODO: Implement saveOrUpdate() method.
    }

    public function delete($id)
    {
        $model = Community::find($id);
        $model->delete();
    }

    public function allWithStatistics()
    {
        return Community::withCount(['houses','families'])->get();
    }
}
