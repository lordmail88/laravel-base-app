<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 7/18/2019
 * Time: 9:44 PM
 */

namespace App\Repositories;


use App\Http\Controllers\NeighborhoodCommunity\CommunityController;
use App\Models\Entity\House;
use Yajra\DataTables\Facades\DataTables;

class HouseRepository implements IHouseRepository
{

    public function all()
    {
        return House::all();
    }

    public function datatables()
    {
        $communityId = session()->get(CommunityController::getSessionKey());
        $data = House::select([
            'id',
            'block',
            'number'
        ])->where(['community_id' => $communityId]);

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($model)
            {
                return '<a href="'.action('UserManagement\UserController@show',['id'=> $model->id]).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a>';
            })
            ->make();
    }

    public function get($id)
    {
        return House::findOrFail($id);
    }

    public function saveOrUpdate($id, \Illuminate\Http\Request $data)
    {
        // TODO: Implement saveOrUpdate() method.
    }

    public function delete($id)
    {
        // TODO: Implement delete() method.
    }
}
