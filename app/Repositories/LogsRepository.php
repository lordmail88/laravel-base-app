<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 7/17/2019
 * Time: 12:15 PM
 */

namespace App\Repositories;


use Illuminate\Support\Facades\File;

class LogsRepository implements ILogsRepository
{
    private $logPath;

    public function __construct()
    {
        $this->logPath = storage_path('logs');
    }

    public function getLogs()
    {
        // TODO: Implement getLogs() method.
        return File::allFiles($this->logPath);
    }

    public function getLog($file)
    {
        return File::get($this->logPath.'/'.$file);
    }
}
