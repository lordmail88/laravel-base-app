<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 6/29/2019
 * Time: 6:21 PM
 */

namespace App\Repositories;


use App\Models\Entity\User;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class UserRepository implements IUserRepository
{

    public function all()
    {
        // TODO: Implement all() method.
        return User::all();
    }

    public function datatables()
    {
        // TODO: Implement datatables() method.
        $data = User::select([
            'id',
            'name',
            'email'
        ]);

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($model)
            {
                return '<a href="'.action('UserManagement\UserController@show',['id'=> $model->id]).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a>';
            })
            ->make();
    }

    public function get($id)
    {
        return User::findOrFail($id);
    }

    public function saveOrUpdate($id, \Illuminate\Http\Request $data)
    {
        if($id == null){
            // New
            $model = new User();
            $model->name = $data->name;
            $model->password = Hash::make($data->password);
            $model->email = $data->email;
            $model->save();
        }else{
            // Update
        }
    }

    public function delete($id)
    {
        $model = User::find($id);
        $model->delete();
    }
}
