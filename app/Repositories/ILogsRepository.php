<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 7/17/2019
 * Time: 12:14 PM
 */

namespace App\Repositories;


interface ILogsRepository
{
    public function getLogs();

    public function getLog($file);
}
