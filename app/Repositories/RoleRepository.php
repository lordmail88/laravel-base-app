<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 7/4/2019
 * Time: 9:57 AM
 */

namespace App\Repositories;


use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class RoleRepository implements IRoleRepository
{

    public function all()
    {
        return Role::all();
    }

    public function datatables()
    {
        $data = Role::select([
            'id',
            'name'
        ]);

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($model)
            {
                return '<a href="'.action('UserManagement\RoleController@show',['id'=> $model->id]).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a>';
            })
            ->make();

    }

    public function get($id)
    {
        return Role::findById($id);
    }

    public function saveOrUpdate($id, \Illuminate\Http\Request $data)
    {
        // TODO: Implement saveOrUpdate() method.
    }

    public function delete($id)
    {
        $model = User::find($id);
        $model->delete();
    }
}
