<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 7/17/2019
 * Time: 10:31 AM
 */

namespace App\Repositories;


interface IAuditTrailRepository
{
    public function getAllModels();

    public function getModel($model);

    public function getDataCount($model);

    public function getAuditCount($model);

    public function getAuditTrail($model);

}
