<?php
/**
 * Created by IntelliJ IDEA.
 * User: Toni Ismail
 * Date: 6/29/2019
 * Time: 6:18 PM
 */

namespace App\Repositories;


interface IBaseCrudRepository
{
    public function all();

    public function datatables();

    public function get($id);

    public function saveOrUpdate($id, \Illuminate\Http\Request $data);

    public function delete($id);

}
