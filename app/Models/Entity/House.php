<?php

namespace App\Models\Entity;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class House extends Model
{
    use ModelUuid;
    //
    protected $table = 'houses';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function community(){
        return $this->belongsTo('App\Models\Entity\Community', 'community_id');
    }

    public function houseInformation(){
        return $this->hasOne('App\Models\Entity\HouseInformation', 'houses_id');
    }

    public function families(){
        return $this->hasMany('App\Models\Entity\Family', 'houses_id');
    }

    public function familyMembers()
    {
        return $this->hasManyThrough(
            FamilyMember::class,
            Family::class,
            'houses_id',
            'family_id',
            'id',
            'id'
        );
    }
}
