<?php

namespace App\Models\Entity;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class FileManager extends Model
{
    use ModelUuid;
    //
    protected $table = 'file_managers';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';
}
