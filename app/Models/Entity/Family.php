<?php

namespace App\Models\Entity;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    use ModelUuid;
    //
    protected $table = 'family';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function house(){
        return $this->belongsTo('App\Models\Entity\House', 'houses_id');
    }

    public function familyMembers(){
        return $this->hasMany('App\Models\Entity\FamilyMember', 'family_id');
    }
}
