<?php

namespace App\Models\Entity;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    use ModelUuid;
    //
    protected $table = 'person';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function familyMember(){
        return $this->belongsTo('App\Models\Entity\FamilyMember', 'family_members_id');
    }
}
