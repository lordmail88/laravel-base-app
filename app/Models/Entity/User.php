<?php

namespace App\Models\Entity;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Str;
use App\Traits\ModelUuid;
use OwenIt\Auditing\Contracts\Auditable;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\Entity\User
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements Auditable
{
    use Notifiable;
    use ModelUuid;
    use HasRoles;
    use \OwenIt\Auditing\Auditable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public $incrementing = false;

    protected $keyType = 'string';

    protected $primaryKey = 'id';
}
