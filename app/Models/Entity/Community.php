<?php

namespace App\Models\Entity;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class Community extends Model
{
    use ModelUuid;
    //
    protected $table = 'community';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function communityStructures(){
        return $this->hasMany('App\Models\Entity\CommunityStructures', 'community_id');
    }

    public function houses(){
        return $this->hasMany('App\Models\Entity\House', 'community_id');
    }

    public function families(){
        return $this->hasManyThrough(Family::class,House::class, 'community_id', 'houses_id', 'id', 'id');
    }

    public function familyMembers()
    {
        $count = collect();
        foreach($this->houses as $house)
        {
            $count->push(count($house->familyMembers));
        }
        return $count->sum();
    }
}
