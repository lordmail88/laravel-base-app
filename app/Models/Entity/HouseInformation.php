<?php

namespace App\Models\Entity;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class HouseInformation extends Model
{
    use ModelUuid;
    //
    protected $table = 'house_informations';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function house(){
        return $this->belongsTo('App\Models\Entity\House', 'houses_id');
    }
}
