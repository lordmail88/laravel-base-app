<?php

namespace App\Models\Entity;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class CommunityStructures extends Model
{
    use ModelUuid;
    //
    protected $table = 'community_structures';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function community(){
        return $this->belongsTo('App\Models\Entity\Community', 'community_id');
    }
}
