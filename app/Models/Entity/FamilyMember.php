<?php

namespace App\Models\Entity;

use App\Traits\ModelUuid;
use Illuminate\Database\Eloquent\Model;

class FamilyMember extends Model
{
    use ModelUuid;
    //
    protected $table = 'family_members';
    public $incrementing = false;
    protected $keyType = 'string';
    protected $primaryKey = 'id';

    public function family(){
        return $this->belongsTo('App\Models\Entity\Family', 'family_id');
    }

    public function person(){
        return $this->hasOne('App\Models\Entity\Person','family_members_id');
    }
}
