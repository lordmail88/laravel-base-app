<?php

namespace App\Providers;

use App\Models\Entity\Community;
use App\Repositories\AuditTrailRepository;
use App\Repositories\CommunityRepository;
use App\Repositories\HouseRepository;
use App\Repositories\IAuditTrailRepository;
use App\Repositories\ICommunityRepository;
use App\Repositories\IHouseRepository;
use App\Repositories\ILogsRepository;
use App\Repositories\LogsRepository;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        /*// Binding User Repository
        $this->app->bind(
            'App\Repositories\IUserRepository',
            'App\Repositories\UserRepository'
        );

        // Binding Role Repository
        $this->app->bind(
            'App\Repositories\IRoleRepository',
            'App\Repositories\RoleRepository'
        );

        // Binding Audit Trail Repository
        $this->app->bind(
            IAuditTrailRepository::class,
            AuditTrailRepository::class
        );

        // Binding Audit Trail Repository
        $this->app->bind(
            ILogsRepository::class,
            LogsRepository::class
        );

        // Binding Community Repository
        $this->app->bind(
            ICommunityRepository::class,
            CommunityRepository::class
        );

        // Binding Community Repository
        $this->app->bind(
            IHouseRepository::class,
            HouseRepository::class
        );*/

        $this->autoRegisterRepository();
    }

    /*
     * Auto Registering Repository on Namespace 'App\Repositories'
     * its valid only if interface implementation on 1 class
     * Otherwise you must register manually
     */
    public function autoRegisterRepository()
    {
        $repoNamespace = 'App\\Repositories';
        $repos = File::allFiles(base_path($repoNamespace));
        foreach ($repos as $repo)
        {
//            Log::info($repo->getRelativePathname());

            $className = str_replace('.php','',$repo->getRelativePathname());
            $ref = new \ReflectionClass($repoNamespace . '\\' . $className);
//            Log::info($ref->isInterface());
            if(!$ref->isInterface() && count($ref->getInterfaceNames()) > 0)
            {
                $iface = $ref->getInterfaceNames()[0];
                $implementation = $ref->getName();

                Log::info($iface ." - ". $implementation);
                $this->app->bind($iface,$implementation);
            }
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        Permission::retrieved(function (Permission $permission) {
            $permission->incrementing = false;
        });

        Permission::creating(function (Permission $permission) {
            $permission->incrementing = false;
            $permission->id = (string) Str::uuid();
        });

        Role::retrieved(function (Role $role) {
            $role->incrementing = false;
        });

        Role::creating(function (Role $role) {
            $role->incrementing = false;
            $role->id = (string) Str::uuid();
        });
    }
}
