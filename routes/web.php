<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


// User Route
Route::get('/user-management/user', 'UserManagement\UserController@index')->name('user.index');
Route::get('/user-management/user/src/datatables', 'UserManagement\UserController@datatables');
Route::get('/user-management/user/src/select2', 'UserManagement\UserController@select2');
Route::get('/user-management/user/new', 'UserManagement\UserController@create');
Route::post('/user-management/user/new', 'UserManagement\UserController@store');
Route::get('/user-management/user/{user}', 'UserManagement\UserController@show');
Route::get('/user-management/user/{user}/edit', 'UserManagement\UserController@edit');
Route::post('/user-management/user/{user}/edit', 'UserManagement\UserController@update');
Route::get('/user-management/user/{user}/delete', 'UserManagement\UserController@delete');
Route::delete('/user-management/user/{user}/delete', 'UserManagement\UserController@destroy');

// Role Route
Route::get('/user-management/role', 'UserManagement\RoleController@index')->name('role.index');
Route::get('/user-management/role/src/datatables', 'UserManagement\RoleController@datatables');
Route::get('/user-management/role/src/select2', 'UserManagement\RoleController@select2');
Route::get('/user-management/role/new', 'UserManagement\RoleController@create');
Route::post('/user-management/role/new', 'UserManagement\RoleController@store');
Route::get('/user-management/role/{id}', 'UserManagement\RoleController@show');
Route::get('/user-management/role/{id}/edit', 'UserManagement\RoleController@edit');
Route::post('/user-management/role/{id}/edit', 'UserManagement\RoleController@update');
Route::get('/user-management/role/{id}/delete', 'UserManagement\RoleController@delete');
Route::delete('/user-management/role/{id}/delete', 'UserManagement\RoleController@destroy');

// Community Route
Route::get('/neighborhood-community/community', 'NeighborhoodCommunity\CommunityController@index')->name('community.index');
Route::get('/neighborhood-community/community/src/datatables', 'NeighborhoodCommunity\CommunityController@datatables');
Route::get('/neighborhood-community/community/src/select2', 'NeighborhoodCommunity\CommunityController@select2');
Route::get('/neighborhood-community/community/new', 'NeighborhoodCommunity\CommunityController@create');
Route::post('/neighborhood-community/community/new', 'NeighborhoodCommunity\CommunityController@store');
Route::get('/neighborhood-community/community/{community}', 'NeighborhoodCommunity\CommunityController@show');
Route::get('/neighborhood-community/community/{community}/edit', 'NeighborhoodCommunity\CommunityController@edit');
Route::post('/neighborhood-community/community/{community}/edit', 'NeighborhoodCommunity\CommunityController@update');
Route::get('/neighborhood-community/community/{community}/delete', 'NeighborhoodCommunity\CommunityController@delete');
Route::delete('/neighborhood-community/community/{community}/delete', 'NeighborhoodCommunity\CommunityController@destroy');

// House Route
Route::get('/neighborhood-community/house', 'NeighborhoodCommunity\HouseController@index')->name('house.index');
Route::get('/neighborhood-community/house/src/datatables', 'NeighborhoodCommunity\HouseController@datatables');
Route::get('/neighborhood-community/house/new', 'NeighborhoodCommunity\HouseController@create');
Route::post('/neighborhood-community/house/new', 'NeighborhoodCommunity\HouseController@store');
Route::get('/neighborhood-community/house/{house}', 'NeighborhoodCommunity\HouseController@show');
Route::get('/neighborhood-community/house/{house}/edit', 'NeighborhoodCommunity\HouseController@edit');
Route::post('/neighborhood-community/house/{house}/edit', 'NeighborhoodCommunity\HouseController@update');
Route::get('/neighborhood-community/house/{house}/delete', 'NeighborhoodCommunity\HouseController@delete');
Route::delete('/neighborhood-community/house/{house}/delete', 'NeighborhoodCommunity\HouseController@destroy');


// Audit Trail
Route::get('/maintenance/audit-trail', 'Maintenance\AuditTrailController@index')->name('audit.trail.index');

// Logs Route
Route::get('/maintenance/logs', 'Maintenance\LogsController@index')->name('logs.index');
Route::get('/maintenance/logs/{log}', 'Maintenance\LogsController@show');
