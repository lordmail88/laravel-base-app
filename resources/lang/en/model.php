<?php

    return [
        'user' => [
            'user' => 'user',
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'password' => 'Password',
            'repassword' => 'Re-Ente Password',
            'verify' => 'Verification Email'
        ],
        'role' => [
            'role' => 'Role',
            'id' => 'ID',
            'name' => 'Name'
        ]
    ];
