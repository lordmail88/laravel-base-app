<?php

    return [
        'data' => ':title Data',
        'create.new' => 'Create New :title',
        'cancel' => 'Cancel',
        'submit' => 'Submit',
        'save' => 'Save',
        'Update' => 'Update',
        'Delete' => 'Delete'
    ];
