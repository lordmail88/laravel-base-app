@extends('adminlte::page')

@section('title', 'Maintenance :: Logs')

@section('content_header')
    <h1>{{ __('model.logs') }}</h1>
@stop

@section('content')
    <div class="row">
        @foreach($logs as $log)
            <div class="col-md-3">
                <div class="box {{ Faker\Factory::create()->randomElement(['box-default','box-primary','box-info','box-warning','box-success','box-danger']) }}">
                    <div class="box-header">
                        <h3 class="box-title">
                            {{ $log->getRelativePathname() }}
                        </h3>
                        <div class="box-tools pull-right">
                            <a href="{{ action('Maintenance\LogsController@show', ['log' => $log->getRelativePathname() ])  }}" class="btn btn-box-tool">
                                <i class="fa fa-external-link"></i>
                            </a>
                        </div>
                    </div>
                    <div class="box-body">

                    </div>
                </div>
            </div>
        @endforeach
    </div>
@stop
