@extends('adminlte::page')

@section('title', 'Maintenance :: Logs')

@section('content_header')
    <h1>{{ __('model.logs') }} <small>{{ $filename }} </small></h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    {!! nl2br ($contents) !!}
                </div>
            </div>
        </div>
    </div>
@stop
