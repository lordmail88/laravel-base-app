@extends('adminlte::page')

@section('title', 'Maintenance :: Audit Trail')

@section('content_header')
    <h1>{{ __('model.audit.trail') }}</h1>
@stop

@section('content')
    <div class="row">
        @foreach($models as $model)
        <div class="col-md-3">
            <div class="box {{ Faker\Factory::create()->randomElement(['box-default','box-primary','box-info','box-warning','box-success','box-danger']) }}">
                <div class="box-header">
                    <h3 class="box-title">
                        {{ $model->name }}
                    </h3>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-sm-6 border-right">
                            <div class="description-block">
                                <div class="description-header">{{ $model->dataCount  }}</div>
                                <div class="description-text">Data</div>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="description-block">
                                <div class="description-header">{{ $model->auditCount  }}</div>
                                <div class="description-text">Audit</div>
                            </div>
                        </div>
                    </div>
                </div>
                @if(!$model->isAudit)
                    <div class="overlay">

                    </div>
                @endif
            </div>
        </div>
        @endforeach
    </div>
@stop
