@extends('adminlte::page')

@section('title', 'Neighborhood Community :: Community')

@section('content_header')
    <h1>{{ __('model.community.community') }}</h1>
@stop

@section('content')
    <div class="row">
        @foreach($models as $model)
            <div class="col-md-4">
                <div class="box box-widget widget-user">
                    <div class="widget-user-header bg-aqua-active">
                        <h3 class="widget-user-username">
                            {{ $model->name }}
                        </h3>
                        <h5 class="widget-user-desc">

                        </h5>
                    </div>
                    <div class="widget-user-image">

                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        {{ $model->houses_count }}
                                    </h5>
                                    <span class="description-text">
                                        Houses
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        {{ $model->families_count }}
                                    </h5>
                                    <span class="description-text">
                                        Families
                                    </span>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header">
                                        {{ $model->familyMembers() }}
                                    </h5>
                                    <span class="description-text">
                                        Family Members
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                        <a href="{{ action('NeighborhoodCommunity\CommunityController@show', ['community' => $model ])  }}" class="btn btn-primary btn-block">Show</a>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

@stop

@section('js')

@stop
