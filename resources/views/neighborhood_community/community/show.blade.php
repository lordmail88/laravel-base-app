@extends('adminlte::page')

@section('title', 'Neighborhood Community :: Community')

@section('content_header')
    <h1>{{ __('model.community.community') }} <small>{{ $model->name }}</small> </h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1" data-toggle="tab" aria-expanded="false">Organization Structure</a>
                    </li>
                    <li>
                        <a href="#tab_2" data-toggle="tab" aria-expanded="false">House</a>
                    </li>
                    <li>
                        <a href="#tab_3" data-toggle="tab" aria-expanded="false">Family</a>
                    </li>
                    <li>
                        <a href="#tab_4" data-toggle="tab" aria-expanded="false">Family Member</a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1"></div>
                    <div class="tab-pane" id="tab_2"></div>
                    <div class="tab-pane" id="tab_3"></div>
                    <div class="tab-pane" id="tab_4"></div>
                </div>
            </div>
        </div>
    </div>
@stop
