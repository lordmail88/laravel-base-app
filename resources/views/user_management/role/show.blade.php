@extends('adminlte::page')

@section('title', 'User Management :: User Detail')

@section('content_header')
    <h1>{{ __('model.user.user') }} <small>{{ $model->name }} </small></h1>
@stop

@section('content')
    <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                ...
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    <table class="table table-striped">
                        <tr>
                            <th>{{ __('model.user.id') }}</th>
                            <td>{{ $model->id }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('model.user.name') }}</th>
                            <td>{{ $model->name }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('model.user.email') }}</th>
                            <td>{{ $model->email }}</td>
                        </tr>
                        <tr>
                            <th>{{ __('model.user.verify') }}</th>
                            <td>{{ blank($model->email_verified_at) ? 'false' : 'true' }}</td>
                        </tr>
                    </table>
                </div>
                <div class="box-footer">
                    <a href="{{ action('UserManagement\UserController@index') }}" class="btn btn-default">
                        {{ __('messages.back') }}
                    </a>
                    <a href="{{ action('UserManagement\UserController@edit', ['id' => $model->id]) }}" class="btn btn-warning">
                        {{ __('messages.edit') }}
                    </a>
                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target=".bs-example-modal-sm">
                        {{ __('messages.delete') }}
                    </button>

                </div>
            </div>
        </div>
    </div>
@stop

@section('js')
    <script>
        $(function(){
            $('.modal').on('shown.bs.modal', function(e){
                $(e.target).find('.modal-content').load('{{ action('UserManagement\UserController@delete', ['id'=> $model->id ]) }}')
            });
        });
    </script>
@stop
