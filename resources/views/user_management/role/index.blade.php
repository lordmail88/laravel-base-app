@extends('adminlte::page')

@section('title', 'User Management :: Role')

@section('content_header')
    <h1>{{ __('model.role.role') }}</h1>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        {{ __('messages.data', ['title'=> __('model.role.role') ]) }}
                    </h3>
                </div>
                <div class="box-body">
                    <a href="{{ action('UserManagement\UserController@create') }}" class="btn btn-primary">{{ __('messages.create.new', ['title'=> __('model.role.role')]) }}</a>
                    <br><br>
                    <table id="table" class="table table-bordered" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>{{ __('model.role.id') }}</th>
                            <th>{{ __('model.role.name') }}</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="box-footer">

                </div>
            </div>
        </div>
    </div>

@stop

@section('js')
    <script>
        $(function () {
            $('#table').DataTable({
                serverSide: true,
                processing: true,
                ajax: '{{ action('UserManagement\RoleController@datatables') }}',
                columns: [
                    {data: 'DT_RowIndex', orderable: false, searchable: false},
                    {data: 'id'},
                    {data: 'name'},
                    {data: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
@stop
