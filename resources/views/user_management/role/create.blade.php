@extends('adminlte::page')

@section('title', 'User Management :: Create New User')

@section('content_header')
    <h1>{{ __('messages.create.new', ['title' => __('model.user.user') ]) }}</h1>
@stop

@section('content')

    {{ BootForm::open() }}


    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                    {{ BootForm::text('name', __('model.user.name')) }}

                    {{ BootForm::email('email', __('model.user.email')) }}

                    {{ BootForm::password('password', __('model.user.password')) }}

                    {{ BootForm::password('repassword', __('model.user.repassword')) }}
                </div>
                <div class="box-footer">
                    {{ BootForm::submit('Save') }}
                </div>
            </div>
        </div>
    </div>

    {{ BootForm::close() }}

@stop
