<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Entity\Community;
use Faker\Generator as Faker;

$factory->define(Community::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->name
    ];
});
