<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Models\Entity\House;
use Faker\Generator as Faker;

$factory->define(House::class, function (Faker $faker) {
    $communityIds = \App\Models\Entity\Community::all()->map(function($model){
        return $model->id;
    });
    return [
        'block'=> $faker->randomElement(array('RF 1','RF 2','RF 3','RF 4','RF 5','RF 6')),
        'number' => $faker->numberBetween(1,200),
        'community_id' => $faker->randomElement($communityIds)
    ];
});
