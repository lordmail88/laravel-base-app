<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBaseNeighborhoodCommunityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // File manager
        Schema::create('file_managers', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('filename');
            $table->text('path');
            $table->timestamps();
        });

        // Community Table
        Schema::create('community', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->timestamps();
        });

        // community structures table
        Schema::create('community_structures', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->uuid('community_structures_id');
            $table->uuid('community_id');
            $table->timestamps();
        });

        // house table
        Schema::create('houses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('block');
            $table->integer('number');
            $table->uuid('community_id');
            $table->timestamps();
        });

        // house informations table
        Schema::create('house_informations', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->bigInteger('surface_area');
            $table->bigInteger('electrical_power');
            $table->integer('number_of_floors');
            $table->integer('number_of_cars');
            $table->integer('number_of_motorcycles');
            $table->uuid('houses_id');
            $table->timestamps();
        });

        // Family
        Schema::create('family', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('family_card_id');
            $table->uuid('houses_id');
            $table->timestamps();
        });

        // Family Members
        Schema::create('family_members', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->enum('family_relationship', ['HEAD_OF_FAMILY','HUSBAND','WIFE','CHILD','DAUGHTER_IN_LAW','GRANDSON','PARENTS','IN_LAWS','ANOTHER_FAMILY','MAID','OTHERS']);
            $table->enum('last_education', ['NOT_IN_SCHOOL','NOT_YET_GRADUATED_FROM_ELEMENTARY_SCHOOL_EQUIVALENT','ELEMENTARY_SCHOOL_EQUIVALENT','MIDDLE_SCHOOL_EQUIVALENT','DIPLOMA_I_II','ACADEMY_DIPLOMA_III_BACHELOR','DIPLOMA_IV_LITERATURE_I','LITERATURE_II','LITERATURE_III']);
            $table->uuid('family_id');
            $table->timestamps();
        });

        // Person
        Schema::create('person', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->enum('gender', ['MALE','FEMALE']);
            $table->enum('blood_group', ['A','B','AB','O','A+','B+']);
            $table->enum('religion', ['ISLAM','CHRISTIAN','CATHOLIC','HINDUISM','BUDDHA','OTHERS']);
            $table->enum('marital_status', ['NOT_MARRIED','MARRIED','DIVORCED_LIFE','DIVORCED_DEAD']);
            $table->string('birth_place');
            $table->date('birth_date');
            $table->uuid('family_members_id');
            $table->timestamps();
        });

        /**
         * Constraints
         **/

        Schema::table('community_structures', function (Blueprint $table){
            $table->foreign('community_id')
                ->references('id')
                ->on('community')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->foreign('community_structures_id')
                ->references('id')
                ->on('community_structures')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('houses', function (Blueprint $table){
            $table->foreign('community_id')
                ->references('id')
                ->on('community')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });

        Schema::table('house_informations', function (Blueprint $table){
            $table->foreign('houses_id')
                ->references('id')
                ->on('houses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('family', function (Blueprint $table){
            $table->foreign('houses_id')
                ->references('id')
                ->on('houses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('family_members', function (Blueprint $table){
            $table->foreign('family_id')
                ->references('id')
                ->on('family')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });

        Schema::table('person', function (Blueprint $table){
            $table->foreign('family_members_id')
                ->references('id')
                ->on('family_members')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
         * Drop Foreign
         */
        Schema::table('person', function (Blueprint $table) {
            $table->dropForeign('person_family_members_id_foreign');
        });
        Schema::table('family_members', function (Blueprint $table) {
            $table->dropForeign('family_members_family_id_foreign');
        });
        Schema::table('family', function (Blueprint $table) {
            $table->dropForeign('family_houses_id_foreign');
        });
        Schema::table('house_informations', function (Blueprint $table) {
            $table->dropForeign('house_informations_houses_id_foreign');
        });
        Schema::table('houses', function (Blueprint $table) {
            $table->dropForeign('houses_community_id_foreign');
        });
        Schema::table('community_structures', function (Blueprint $table) {
            $table->dropForeign('community_structures_community_id_foreign');
            $table->dropForeign('community_structures_community_structures_id_foreign');
        });


        Schema::dropIfExists('person');
        Schema::dropIfExists('family_members');
        Schema::dropIfExists('family');
        Schema::dropIfExists('house_informations');
        Schema::dropIfExists('houses');
        Schema::dropIfExists('community_structures');
        Schema::dropIfExists('community');
        Schema::dropIfExists('file_managers');
    }
}
