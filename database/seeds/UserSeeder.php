<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users = factory(App\Models\Entity\User::class, 1)->create();

        $user = $users[0];
        $this->command->info($user);
    }
}
