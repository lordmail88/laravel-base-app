<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = factory(Spatie\Permission\Models\Role::class, 1)->create();

        $this->command->info($roles);
    }
}
