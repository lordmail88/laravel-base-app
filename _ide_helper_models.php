<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models\Entity{
/**
 * App\Models\Entity\Community
 *
 * @property string $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Entity\CommunityStructures[] $communityStructures
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Entity\House[] $houses
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Community newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Community newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Community query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Community whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Community whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Community whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Community whereUpdatedAt($value)
 */
	class Community extends \Eloquent {}
}

namespace App\Models\Entity{
/**
 * App\Models\Entity\CommunityStructures
 *
 * @property string $id
 * @property string $name
 * @property string $community_structures_id
 * @property string $community_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Entity\Community $community
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures whereCommunityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures whereCommunityStructuresId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\CommunityStructures whereUpdatedAt($value)
 */
	class CommunityStructures extends \Eloquent {}
}

namespace App\Models\Entity{
/**
 * App\Models\Entity\Family
 *
 * @property string $id
 * @property string $family_card_id
 * @property string $houses_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Entity\FamilyMember[] $familyMembers
 * @property-read \App\Models\Entity\House $house
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Family newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Family newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Family query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Family whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Family whereFamilyCardId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Family whereHousesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Family whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Family whereUpdatedAt($value)
 */
	class Family extends \Eloquent {}
}

namespace App\Models\Entity{
/**
 * App\Models\Entity\FamilyMember
 *
 * @property string $id
 * @property string $family_relationship
 * @property string $last_education
 * @property string $family_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Entity\Family $family
 * @property-read \App\Models\Entity\Person $person
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember whereFamilyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember whereFamilyRelationship($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember whereLastEducation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FamilyMember whereUpdatedAt($value)
 */
	class FamilyMember extends \Eloquent {}
}

namespace App\Models\Entity{
/**
 * App\Models\Entity\FileManager
 *
 * @property string $id
 * @property string $filename
 * @property string $path
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FileManager newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FileManager newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FileManager query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FileManager whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FileManager whereFilename($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FileManager whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FileManager wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\FileManager whereUpdatedAt($value)
 */
	class FileManager extends \Eloquent {}
}

namespace App\Models\Entity{
/**
 * App\Models\Entity\House
 *
 * @property string $id
 * @property string $block
 * @property int $number
 * @property string $community_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Entity\Community $community
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Entity\Family[] $families
 * @property-read \App\Models\Entity\HouseInformation $houseInformation
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House whereBlock($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House whereCommunityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\House whereUpdatedAt($value)
 */
	class House extends \Eloquent {}
}

namespace App\Models\Entity{
/**
 * App\Models\Entity\HouseInformation
 *
 * @property string $id
 * @property int $surface_area
 * @property int $electrical_power
 * @property int $number_of_floors
 * @property int $number_of_cars
 * @property int $number_of_motorcycles
 * @property string $houses_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Entity\House $house
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereElectricalPower($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereHousesId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereNumberOfCars($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereNumberOfFloors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereNumberOfMotorcycles($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereSurfaceArea($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\HouseInformation whereUpdatedAt($value)
 */
	class HouseInformation extends \Eloquent {}
}

namespace App\Models\Entity{
/**
 * App\Models\Entity\Person
 *
 * @property string $id
 * @property string $name
 * @property string $gender
 * @property string $blood_group
 * @property string $religion
 * @property string $marital_status
 * @property string $birth_place
 * @property string $birth_date
 * @property string $family_members_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Entity\FamilyMember $familyMember
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereBirthDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereBirthPlace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereBloodGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereFamilyMembersId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereMaritalStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereReligion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\Person whereUpdatedAt($value)
 */
	class Person extends \Eloquent {}
}

namespace App\Models\Entity{
/**
 * App\Models\Entity\User
 *
 * @property string $id
 * @property string $name
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\OwenIt\Auditing\Models\Audit[] $audits
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Entity\User role($roles, $guard = null)
 */
	class User extends \Eloquent {}
}

